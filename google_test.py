import Google_API_func
import requests
import argparse
import datetime

import time
import os
import json

from datetime import datetime, timedelta  
from time import sleep  



parser = argparse.ArgumentParser()
parser.add_argument("--distr", default='HK_island', help="name of District file")
parser.add_argument("--m", type=int, default=25, help="maximum number of vettices in a waypoint request") # maximum value of vertices in a waypoint request    

parser.add_argument("--start_hour", type=int, default=17, help="starthour") # start hour 
parser.add_argument("--start_minute", type=int, default=32, help="start minute") # 
parser.add_argument("--duration", type=int, default=1, help="duration") # 
parser.add_argument("--interval", type=int, default=10, help="interval") # 

# date of history data
parser.add_argument("--day", type=int, default=20, help="day") # 
parser.add_argument("--month", type=int, default=7, help="month") # 
parser.add_argument("--year", type=int, default=2018, help="2018") # 
parser.add_argument("--parse", type=int, default=0, help="0, 1") # 

# start_hour, start_minute, duration, interval
args = parser.parse_args()
#
params = {
         'distr': args.distr,
         'maxNumInwaypointReq': args.m,
         'start_hour': args.start_hour,
         'start_minute': args.start_minute,
         'duration': args.duration,
         'interval': args.interval,
         'day': args.day,
         'month': args.month,
         'year': args.year
         }
#
#-------params--------------------#
#
#------run--------------------------#
if args.parse==1:
   speed_info=Google_API_func.parse_google_json_to_list_by_day(params,1)
else:
#    nowtime = datetime.now()
#    params['day']=nowtime.day
#    params['month']=nowtime.month
#    if (nowtime.minute)%2==1:
#       params['start_minute']=(nowtime.minute+1)%60
#       params['start_hour']=nowtime.hour + int((nowtime.minute+1)/60)
#    else:
#       params['start_minute']=(nowtime.minute+2)%60
#       params['start_hour']=nowtime.hour + int((nowtime.minute+2)/60)
   
   # params['duration']=(23-params['start_hour'])*30 + (60-params['start_minute'])/2
   print('start at:\t%s-%.2d-%.2d %.2d:%.2d:00\nduration is:\t%s\ninterval is:\t%s' % (params['year'], params['month'], params['day'], params['start_hour'], params['start_minute'], params['duration'], params['interval']))
   # exit()
   Google_API_func.google_timer_test(params, 'jti_smp')
print('yes!!!')



