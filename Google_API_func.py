## Google maps application: smp (13 routes) and jti (9 routes)
import math
import argparse
import time
import os
import json
import io
import requests
import numpy as np
import csv

import convert

import random
import googlemaps
import _thread as thread

import datetime
# from datetime import datetime
from time import sleep  

from multiprocessing.pool import Pool, ThreadPool
from threading import Thread,Event

import concurrent.futures
# import urllib.request


from multiprocessing import Pool
from multiprocessing.dummy import Pool as ThreadPool


import matplotlib
# matplotlib.use('Agg')
import numpy as np
import matplotlib.pyplot as plt
plt.switch_backend('agg')

# keys =["AIzaSyDO-0620NhxVBG0rjKZmkDkJ1iTc_e8Vw0", "AIzaSyDO-0620NhxVBG0rjKZmkDkJ1iTc_e8Vw0","AIzaSyDO-0620NhxVBG0rjKZmkDkJ1iTc_e8Vw0","AIzaSyDO-0620NhxVBG0rjKZmkDkJ1iTc_e8Vw0"]
#Google MapsAPI
#------------------------------google maps service (direction)------------------------------------------#
def load_url(url):
    result = {}
    r=requests.get(url)
    result[url] = r
    return result

def get_waypointReqSet_from_lat_lng(flag):
    if flag=='jti':
        csvpath='google_jti_routes.csv'
    if flag=='smp':
        csvpath='google_smp_routes.csv'
    if flag=='jti_smp':
        csvpath='google_jti_smp_routes.csv'
    print(csvpath)
    reader = csv.reader(open(csvpath, 'r'))
    next(reader, None)
    LINK = []
    # LINK.append([])
    for row in reader:
        LINK.append([])
        # print(row)
        # for point in row:
        for i in range(int(row[1])+1):
            LINK[-1].append((row[0],row[2+2*i], row[3+2*i]))
    # for link in LINK:
    #     print(link)
    return LINK

def get_google_direction_urls(params, wayReqSet,key):
    # gmaps=googlemaps.Client(key='AIzaSyDO-0620NhxVBG0rjKZmkDkJ1iTc_e8Vw0')
    #WaypointDirectionApi: , AIzaSyBWEre-1y4STCUgP9con3BXQW4-8Z3wE4w, AIzaSyBsBAJxkhGIc_juAE2-WRpdP4RgS8yI4tg

    # print(wayReqSet)
    # initialize--
    URLS={}
    
    for i in range(len(wayReqSet)):
        for j in range(len(wayReqSet[i])):

            Y=wayReqSet[i][j][1]
            X=wayReqSet[i][j][2] 
            # ID = wayReqSet[i][j]
            if j > 0 and j < len(wayReqSet[i]) - 1:
                if j ==1:
                    googleurl=googleurl   + "&waypoints=via:" + str(Y) + "%2C" + str(X) 
                else:
                    googleurl=googleurl   + "%7Cvia:" + str(Y) + "%2C" + str(X) 
            else:
                if j == 0:
                    googleurl="https://maps.googleapis.com/maps/api/directions/json?origin=" + str(Y) + "," + str(X) 
                if j == (len(wayReqSet[i]) - 1):
                    googleurl= googleurl + "&destination=" + str(Y) + "," + str(X)
        now=datetime.datetime.now()
        nowtime=convert.time(now)
        # googleurl=googleurl + '&mode=driving' + '&departure_time=%s' % nowtime + '&traffic_model=best_guess' + "&sensor=false" + '&key=AIzaSyDO-0620NhxVBG0rjKZmkDkJ1iTc_e8Vw0'
        googleurl=googleurl + '&mode=driving' + '&departure_time=%s' % nowtime + '&traffic_model=best_guess' + "&sensor=false" + '&key=%s' % key 
        # print(googleurl)
        
        URLS[googleurl] =  wayReqSet[i][0][0]
        # https://maps.googleapis.com/maps/api/directions/json?origin=sydney,au&destination=perth,au&waypoints=via:-37.81223%2C144.96254%7Cvia:-34.92788%2C138.60008&mode=driving&departure_time=1517142855&traffic_model=best_guess&sensor=false&key=AIzaSyDO-0620NhxVBG0rjKZmkDkJ1iTc_e8Vw0
    return URLS

def parallel_call_google_direction_urls(URLS, params, timestamp, num):
    # errorFile=open('error_files/error_url_%s.txt' % timestamp, 'w')
    pool=ThreadPool(num)
    # print('yes')
    curTime=datetime.datetime.now()
    urlsList = URLS.keys()
    try:  
        results=pool.map(load_url, urlsList)
        pool.close()
        pool.join()
    except ZeroDivisionError:  
        print("Error")  
    else:  
        # print(result)      # print('yes1')

        # close the pool and wait for the work to finish
        now=datetime.datetime.now()
        ii=-1
        for result in results:
            for key in result:
                res = result[key]
                ii= ii + 1
                if res.status_code== 200:
                    fileId = URLS[key]
                    jsonstr=res.json()
                    contents=json.dumps(jsonstr, indent=4, sort_keys=True, separators=(',', ': '), ensure_ascii=False)
                    # print(contents)    
                    # saveJsonName=('Res/%s/date_%s-%.2d-%.2d/time_%.2d-%.2d/routes/routes%.3d-%.2d-%.2d.json' % (params['distr'], timestamp.year, timestamp.month,timestamp.day, timestamp.hour, timestamp.minute, ii, now.hour, now.minute))
                    saveJsonName=('Res/%s/date_%s-%.2d-%.2d/time_%.2d-%.2d/routes/routes%s-%.2d-%.2d.json' % (params['distr'], timestamp.year, timestamp.month,timestamp.day, timestamp.hour, timestamp.minute, fileId , now.hour, now.minute))
                    directory=os.path.dirname(saveJsonName)
                    # print(directory)
                    if not os.path.exists(directory):
                        os.makedirs(directory, exist_ok=True)
            
                    # save return json file
                    with io.open(saveJsonName, 'w') as jsonfile:
                            jsonfile.write(contents)
                            jsonfile.close()
        #     else:                
    #         errorFile.write('timestamp:\t%s\n' % timestamp)
    #         errorFile.write('numId:\t%s\n' % ii)
    #         errorFile.write('header:\t%s\n' % res.headers['Content-Type'])
    #         errorFile.write('status_code:\t%s\n' % res.status_code)                
    #         errorFile.write(res.text)
    # errorFile.close()

# def run_task(params, wayReqSet):
#     interval=params['interval'] # interval of each time slot
#     duration=params['duration'] # number of time slotss
#     #  run 24 times for 24 time slots (number of duratio)
#     minute0=params['start_minute']
#     start_minute =params['start_minute']
#     start_hour=params['start_hour']
#     for tmpTime in range(int(duration)):
#         timestamp=datetime.datetime.now()
#         print(timestamp)
#         #---------------routes info
#         # URLS=get_google_direction_tsm_urls(params, wayReqSet)

#         URLS=get_google_direction_urls(params, wayReqSet)
#         parallel_call_google_direction_urls(URLS, params, timestamp,1)
        
#         start_minute =start_minute + interval # time interval is 5 minutes
#         tmpMinute =int(start_minute%60 )# munite
#         tmpHour=int(int(start_hour + start_minute/60))%24 # hour                            
#         nextTime=timestamp.replace(hour=tmpHour, minute=tmpMinute, second=0, microsecond=0) # next time slot
#         # print(nextTime)
#         # nextTime=timestamp+datetime.timedelta(minutes=int(interval))
#         curTime=datetime.datetime.now()
#         # print(curTime)
#         if tmpTime < (duration -1):
#             delta=nextTime-curTime # rest time to next slot
#             sleeptime=delta.total_seconds()  # sleep until next time slot
#             sleep(sleeptime)



def run_task(params, wayReqSet,key):
    interval=params['interval'] # interval of each time slot
    duration=params['duration'] # number of time slotss
    #  run 24 times for 24 time slots (number of duratio)
    minute0=params['start_minute']
    start_minute =params['start_minute']
    start_hour=params['start_hour']
    for tmpTime in range(int(duration)):
        timestamp=datetime.datetime.now()
        print(timestamp)
        #---------------routes info
        # URLS=get_google_direction_tsm_urls(params, wayReqSet)

        URLS=get_google_direction_urls(params, wayReqSet,key)
        parallel_call_google_direction_urls(URLS, params, timestamp,1)
        
        start_minute =start_minute + interval # time interval is 5 minutes
        tmpMinute =int(start_minute%60 )# munite
        tmpHour=int(int(start_hour + start_minute/60))%24 # hour                            
        nextTime=timestamp.replace(hour=tmpHour, minute=tmpMinute, second=0, microsecond=0) # next time slot
        # print(nextTime)
        # nextTime=timestamp+datetime.timedelta(minutes=int(interval))
        curTime=datetime.datetime.now()
        # print(curTime)
        if tmpTime < (duration -1):
            delta=nextTime-curTime # rest time to next slot
            sleeptime=delta.total_seconds()  # sleep until next time slot
            sleep(sleeptime)

def google_timer(params, flag):
    # wayReqSet=simplyGraph_1.pre_test_from_file()
    wayReqSet=get_waypointReqSet_from_lat_lng(flag)

    # URLS=get_google_direction_smp_urls(params, wayReqSet)
    sched_Timer = datetime.datetime(params['year'], params['month'], params['day'], params['start_hour'], params['start_minute'], 0)
    weekdays=['Monday','Tuesday','Wednesday','Thursday','Friday']
    flag = 0
    print(sched_Timer)
    while True:
        now = datetime.datetime.now()
        if sched_Timer<now<(sched_Timer+datetime.timedelta(seconds=1)):
            print(sched_Timer)
            flag = 1
        else:
            time.sleep(1)                
        # if flag == 1 and weekdays[sched_Timer.weekday()]:
        if flag == 1:
            sched_Timer=sched_Timer+datetime.timedelta(days=1)
            # sched_Timer.replace(hour=0, minute=0, second=0, microsecond=0)
            sched_Timer=sched_Timer.replace(hour=0, minute=0, second=0, microsecond=0)
            print(sched_Timer)
            run_task(params, wayReqSet)#此处为你自己想定时执行的功能函数
            # params["duration"]=720
            params['start_minute']=sched_Timer.minute
            params['start_hour']=sched_Timer.hour
            params['month']=sched_Timer.month
            params['day']=sched_Timer.day
            print('start at:\t%s-%.2d-%.2d %.2d:%.2d:00\nduration is:\t%s\ninterval is:\t%s' % (params['year'], params['month'], params['day'], params['start_hour'], params['start_minute'], params['duration'], params['interval']))
            flag = 0                
            # params['da']

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def google_timer_test(params, flag):
    # wayReqSet=simplyGraph_1.pre_test_from_file()
    print('----1')
    wayReqSet=get_waypointReqSet_from_lat_lng(flag)
    print('---2')

    NoOfEachPool = math.ceil(len(wayReqSet)/len(keys))

    requests = chunks(wayReqSet,NoOfEachPool);
    requests = list(requests)
    # requests = [x for x in requests]
    requestTable = {}
    results = []
    for idx, val in enumerate(keys):
        requestTable[val] = requests[idx]

    for key in requestTable:
        thread.start_new_thread( runJob, (params,requestTable[key], key,) )
    time.sleep(params['duration']*params['interval']*60)

def runJob(params,requests, key):
    sched_Timer = datetime.datetime(params['year'], params['month'], params['day'], params['start_hour'], params['start_minute'], 0)
    weekdays=['Monday','Tuesday','Wednesday','Thursday','Friday']
    flag = 0
    print(key +" target time at " + str(sched_Timer))
    while True:
        now = datetime.datetime.now()
        if sched_Timer<now<(sched_Timer+datetime.timedelta(seconds=1)):
            print(sched_Timer)
            flag = 1
        else:
            time.sleep(1)                
        # if flag == 1 and weekdays[sched_Timer.weekday()]:
        if flag == 1:
            sched_Timer=sched_Timer+datetime.timedelta(days=1)
            # sched_Timer.replace(hour=0, minute=0, second=0, microsecond=0)
            sched_Timer=sched_Timer.replace(hour=0, minute=0, second=0, microsecond=0)
            print(sched_Timer)
            run_task(params, requests,key)#此处为你自己想定时执行的功能函数
            # params["duration"]=720
            params['start_minute']=sched_Timer.minute
            params['start_hour']=sched_Timer.hour
            params['month']=sched_Timer.month
            params['day']=sched_Timer.day
            print('start at:\t%s-%.2d-%.2d %.2d:%.2d:00\nduration is:\t%s\ninterval is:\t%s' % (params['year'], params['month'], params['day'], params['start_hour'], params['start_minute'], params['duration'], params['interval']))
            flag = 0          
#------------------------------google json parsing (direction)------------------------------------------#
def parse_google_json_to_list_dist(params):
    wayReqSet, Length=get_waypointReqSet_from_tsm_lat_lng()
    
    route_info=[] # store travel time
    for kk in range(len(wayReqSet)):
        route_info.append([])

    hour=params['start_hour']
    minute=params['start_minute']
    interval=params['interval']
    duration=params['duration']

    for i in range(duration):   # number of time slots
        if i > 0:
            minute=minute + interval
            tmpminute =int(minute%60)
            tmphour=int(hour + minute/60)
        else:
            minute=minute
            tmpminute =int(minute)
            tmphour=int(hour)

        tp=0
        for ii in range(len(wayReqSet)): # each routing request
            json_file_path='Res/%s/date_%s-%.2d-%.2d/time_%.2d-%.2d/routes/routes%.3d-%.2d-%.2d.json' % (params['distr'],params['year'],  int(params['month']), int(params['day']), tmphour, tmpminute, ii, tmphour, tmpminute)
            if os.path.isfile(json_file_path):
    # print(json_file_path)
                with open(json_file_path) as jsonFile:
                    jsonstr=json.load(jsonFile)
                    # print(jsonstr)
                if jsonstr['status']=='OK':
                    tp=tp+1
                    legs=jsonstr['routes'][0]['legs']
                    dist = legs[0]['distance']['value']
                    if dist==0:
                        dist = 1
                    route_info[ii].append(float(dist))
                    route_info[ii].append(len(legs[0]['steps']))
        print(tp)

    # print(route_info)
    route_info_path=('json/route_info_%s_%.2d-%.2d-%s.npy' % (params['distr'], int(params['day']), int(params['month']), params['year']))
    directory=os.path.dirname(route_info_path)
        # print(directory)
    if not os.path.exists(directory):
        os.makedirs(directory)

    thefile=open(route_info_path, 'w')
    for item in route_info:
        for tmp in range(len(item)):# print(item)
            if tmp < len(item) - 1:
                thefile.write("%s\t" % item[tmp])
            else:
                thefile.write("%s\n" % item[tmp])

    dist = route_info
    return dist, Length


def test(params):
    # Dist = get_shapelength()
    # print(Dist)
       # API_func.google_timer(params1)
    route_dist, Dist= parse_google_json_to_list_dist(params)
    print(len(route_dist))
    print(len(Dist))
    # print(route_dist)
    cnt1 = 0
    cnt2 = 0
    cnt3 = 0
    
    cnt11 = 0
    cnt22 = 0
    cnt33 = 0
    
    cnt0 = 0
    cnt00 = 0
    unmatchlink=[]
    thefile=open('TSM/file.txt', 'w')
   

    a=30
    b=100
    c=300

    for i in range(len(Dist)):
        if Dist[i][0] > 100:
            diff = abs(int(Dist[i][0]) - int(route_dist[i][0]))/int(Dist[i][0])
            if int(Dist[i][0]) <a:
               cnt0 = cnt0+1
               if abs(int(Dist[i][0]) - int(route_dist[i][0])) > 10: # and int(route_dist[i][1]) > 1
                     cnt00 =  cnt00+ 1
                     unmatchlink.append([i,Dist[i][0], route_dist[i][0]])
            if int(Dist[i][0])>=a and int(Dist[i][0]) < b:
               cnt1 = cnt1 + 1
               if diff > 0.1 and int(route_dist[i][1]) > 1:
                     cnt11 =  cnt11+ 1
                     unmatchlink.append([i,Dist[i][0], route_dist[i][0]])
                     # print(i)
            if int(Dist[i][0])>=b and int(Dist[i][0]) < c:
               cnt2 = cnt2 + 1
               if diff > 0.1:
                  cnt22 = cnt22 + 1 
                  unmatchlink.append([i,Dist[i][0], route_dist[i][0]])
                     # print(i)
            if int(Dist[i][0])>=c:
               cnt3 = cnt3 + 1
               if diff > 0.1:
                  cnt33 = cnt33 + 1
                  unmatchlink.append([i,Dist[i][0], route_dist[i][0]])
    # print(unmatchlink)
            # print(i)
            # unmatchlink.append([i,Dist[i][0], route_dist[i][0]])
            # unmatchlink.append(i)


    for item in unmatchlink:
        thefile.write('%s\n' % item)

    # print('\n')
    # # print('(0-%s)\t%d\t%d\t%s' % (a,cnt0, cnt00, float((cnt0-cnt00)/cnt0)))
    # # print('(%s-%s)\t%d\t%d\t%s' % (a, b, cnt1, cnt11, float((cnt1-cnt11)/cnt1)))
    print('(%s-%s)\t%d\t%d\t%s' % (b, c, cnt2, cnt22, float((cnt2-cnt22)/cnt2)))
    print('(%s+)\t%d\t%d\t%s' % (c, cnt3, cnt33, float((cnt3 - cnt33)/cnt3)))
    
    # sum0=   cnt1+ cnt2 + cnt3
    # sum00=  cnt11 + cnt22 + cnt33
    
    # sum1 = cnt0 + cnt1+ cnt2 + cnt3
    # sum11= cnt00 + cnt11 + cnt22 + cnt33
    # print((1-float(sum00/sum0))*100)
    # print((1-float(sum11/sum1))*100)

def parse_google_json_to_list_new(params, flag):
    # wayReqSet = get_waypointReqSet_from_tsm_csv()
    wayReqSet,length = get_waypointReqSet_from_tsm_lat_lng()
    
    route_info=[] # store travel time
    for kk in range(len(wayReqSet)):
        route_info.append([])    
    speed_info=[] # store travel time
    for kk in range(len(wayReqSet)):
        speed_info.append([])

    hour=params['start_hour']
    minute=params['start_minute']
    interval=params['interval']
    duration=params['duration']
    for i in range(4):   # number of time slots
    
    # for i in range(duration):   # number of time slots
        if i > 0:
            minute=minute + interval
            tmpminute =int(minute%60)
            tmphour=int(hour + minute/60)
        else:
            minute=minute
            tmpminute =int(minute)
            tmphour=int(hour)     

        for j in range(4): #number of days
            params['day'] = 6+j
            tp=0
            for ii in range(len(wayReqSet)): # each routing request
                json_file_path='Res/%s/date_%.2d-%.2d-%s/time_%.2d-%.2d/routes/routes%.3d-%.2d-%.2d.json' % (params['distr'], int(params['day']), int(params['month']), params['year'], tmphour, tmpminute, ii, tmphour, tmpminute)
                # print(json_file_path)
                if os.path.isfile(json_file_path):
                    # print(json_file_path)
                    with open(json_file_path) as jsonFile:
                        jsonstr=json.load(jsonFile)
                        # print(jsonstr)
                    if jsonstr['status']=='OK':
    
                        legs=jsonstr['routes'][0]['legs']
                        if 'steps' in legs[0]:
                            # if len(legs[0]['steps']) > 1:
                            if legs[0]['distance']['value'] > 100:
                                tp=tp+1
                        if 'duration_in_traffic' in legs[0]:
                            durationv = int(legs[0]['duration_in_traffic']['value'])
                            # route_info[ii].append(legs[0]['duration_in_traffic']['value'])# travel time value  
                        else:
                            durationv = int(legs[0]['duration']['value'])
                        
                        if durationv == 0:
                            durationv=1
                        speed= int(legs[0]['distance']['value'])/duration
                        
                        if speed==0:
                            speed=20
                        
                        # speed_info[ii].append(speed*3.6)# travel time value  
                        speed_info[ii].append(math.log(speed*3.6))# travel time value  
                        route_info[ii].append(duration)# travel time value          
    
            print('---------%s----------' % tp)

    
    print(len(route_info))
    route_info_path=('json/all/duration/duration_info_%s_%.2d-%.2d-%s.npy' % (params['distr'], int(params['day']), int(params['month']), params['year']))
    speed_info_path=('json/all/speed/speed_info_%s_%.2d-%.2d-%s.npy' % (params['distr'], int(params['day']), int(params['month']), params['year']))
    directory=os.path.dirname(route_info_path)
        # print(directory)
    if not os.path.exists(directory):
        os.makedirs(directory)        
    directory=os.path.dirname(speed_info_path)
        # print(directory)
    if not os.path.exists(directory):
        os.makedirs(directory)        
    thefile=open(route_info_path, 'w')
    for item in route_info:
        for tmp in range(len(item)):# print(item)
            if tmp < len(item) - 1:
                thefile.write("%s\t" % item[tmp])
            else:
                thefile.write("%s\n" % item[tmp])                    
    thefile=open(speed_info_path, 'w')
    for item in speed_info:
        for tmp in range(len(item)):# print(item)
            if tmp < len(item) - 1:
                thefile.write("%s\t" % item[tmp])
            else:
                thefile.write("%s\n" % item[tmp])
    cnt=-1
    tmpp=0
    for x in route_info:
        cnt = cnt + 1
        std_value = np.std(x)
        avg_value=np.mean(x)
        if std_value/avg_value < 0.5:
            tmpp=tmpp+1
            # print('edge_id:\t%s' % cnt) #edge_id
            # print('std_value:\t%s' % std_value)
            # print(std_value/avg_value)
            # print(x)
            # fig, ax = plt.subplots( nrows=1, ncols=1 )  # create figure & 1 axis
            # ax.plot(x)
            # fig.savefig('Res/img/%s_%s.png' % (params['distr'], cnt))  # save the figure to file
            # plt.close(fig)  
    # print(route_info)
    print(tmpp)

    return route_info


def parse_google_json_to_list_by_time_slots(params):
    print('parse json starting...')
    # if flag==1:
    #     wayReqSet=simplyGraph_1.pre_test_from_file() # yaotsimmong
    # else:
    #     wayReqSet=simplyGraph_2.pre_test_from_file()
    # wayReqSet = get_waypointReqSet_from_tsm_csv()
    wayReqSet=get_waypointReqSet_from_tsm_lat_lng()
    
    hour=params['start_hour']
    minute=params['start_minute']
    interval=params['interval']
    duration=params['duration']

    for i in range(duration):   # number of time slots
        if i > 0:
            minute=minute + interval
            tmpminute =int(minute%60)
            tmphour=int(hour + minute/60)
        else:
            minute=minute
            tmpminute =int(minute)
            tmphour=int(hour)
        

        route_info=[] # store travel time
        for kk in range(len(wayReqSet)):
            route_info.append([])
    
        speed_info=[] # store travel time
        for kk in range(len(wayReqSet)):
            speed_info.append([])


        for j in range(4):
            params['day'] = 6+j
            tp=0
            for ii in range(len(wayReqSet)): # each routing request
            # for ii in range(1): # each routing request
                # if ii in [0,230,233,41,194,203]:
                # json_file_path='Res/%s/date_%.2d-%.2d-%s/time_%.2d-%.2d/routes/routes%.3d-%.2d-%.2d.json' % (params['distr'], int(params['day']), int(params['month']), params['year'], tmpminute, tmphour, ii, tmphour, tmpminute)
                json_file_path='Res/%s/date_%.2d-%.2d-%s/time_%.2d-%.2d/routes/routes%.3d-%.2d-%.2d.json' % (params['distr'], int(params['day']), int(params['month']), params['year'], tmphour, tmpminute, ii, tmphour, tmpminute)
                print(json_file_path)
                if os.path.isfile(json_file_path):
                    print(json_file_path)
                    with open(json_file_path) as jsonFile:
                        jsonstr=json.load(jsonFile)
                        # print(jsonstr)
                    if jsonstr['status']=='OK':
                        # tp=tp+1
                        # print(jsonstr)
                        # print(jsonstr['routes'][0])
    
                        legs=jsonstr['routes'][0]['legs']
                        if 'steps' in legs[0]:
                            # if len(legs[0]['steps']) > 1:
                            if legs[0]['distance']['value'] > 100:
                                tp=tp+1
                        if 'duration_in_traffic' in legs[0]:
                            duration = int(legs[0]['duration_in_traffic']['value'])
                            # route_info[ii].append(legs[0]['duration_in_traffic']['value'])# travel time value  
                        else:
                            duration = int(legs[0]['duration']['value'])
                        
                        if duration == 0:
                            duration=1
                        speed= int(legs[0]['distance']['value'])/duration
                        
                        if speed==0:
                            speed=20
                        
                        # speed_info[ii].append(speed*3.6)# travel time value  
                        speed_info[ii].append(math.log(speed*3.6))# travel time value  
                        route_info[ii].append(duration)# travel time value          
    
            print('---------%s----------' % tp)

    
        # print(route_info)
        route_info_path=('json/slots/%.2d/duration/duration_info_%s_%.2d-%.2d-%s.npy' % (i,params['distr'], int(params['day']), int(params['month']), params['year']))
        speed_info_path=('json/slots/%.2d/speed/speed_info_%s_%.2d-%.2d-%s.npy' % (i,params['distr'], int(params['day']), int(params['month']), params['year']))
        directory=os.path.dirname(route_info_path)
            # print(directory)
        if not os.path.exists(directory):
            os.makedirs(directory)        
        directory=os.path.dirname(speed_info_path)
            # print(directory)
        if not os.path.exists(directory):
            os.makedirs(directory)        
        thefile=open(route_info_path, 'w')
        for item in route_info:
            for tmp in range(len(item)):# print(item)
                if tmp < len(item) - 1:
                    thefile.write("%s\t" % item[tmp])
                else:
                    thefile.write("%s\n" % item[tmp])                    
        thefile=open(speed_info_path, 'w')
        for item in speed_info:
            for tmp in range(len(item)):# print(item)
                if tmp < len(item) - 1:
                    thefile.write("%s\t" % item[tmp])
                else:
                    thefile.write("%s\n" % item[tmp])
        cnt=-1
        tmpp=0
        for x in route_info:
            cnt = cnt + 1
            std_value = np.std(x)
            avg_value=np.mean(x)
            if std_value/avg_value < 0.5:
                tmpp=tmpp+1
                # print('edge_id:\t%s' % cnt) #edge_id
                # print('std_value:\t%s' % std_value)
                # print(std_value/avg_value)
                # print(x)
                # fig, ax = plt.subplots( nrows=1, ncols=1 )  # create figure & 1 axis
                # ax.plot(x)
                # fig.savefig('Res/img/%s_%s.png' % (params['distr'], cnt))  # save the figure to file
                # plt.close(fig)  
        # print(route_info)
        print(tmpp)

    return route_info


def parse_google_json_to_list(params):
    # if flag==1:
    #     wayReqSet=simplyGraph_1.pre_test_from_file()
    # else:
    #     wayReqSet=simplyGraph_2.pre_test_from_file()
    wayReqSet = get_waypointReqSet_from_tsm_csv()
    
    route_info=[] # store travel time
    for kk in range(len(wayReqSet)):
        route_info.append([])

    speed_info=[] # store travel time
    for kk in range(len(wayReqSet)):
        speed_info.append([])

    hour=params['start_hour']
    minute=params['start_minute']
    interval=params['interval']
    duration=params['duration']

    for i in range(duration):   # number of time slots
        if i > 0:
            minute=minute + interval
            tmpminute =int(minute%60)
            tmphour=int(hour + minute/60)
        else:
            minute=minute
            tmpminute =int(minute)
            tmphour=int(hour)

        tp=0
        for ii in range(len(wayReqSet)): # each routing request
        # for ii in range(1): # each routing request
            # if ii in [0,230,233,41,194,203]:
            # json_file_path='Res/%s/date_%.2d-%.2d-%s/time_%.2d-%.2d/routes/routes%.3d-%.2d-%.2d.json' % (params['distr'], int(params['day']), int(params['month']), params['year'], tmpminute, tmphour, ii, tmphour, tmpminute)
            json_file_path='Res/%s/date_%.2d-%.2d-%s/time_%.2d-%.2d/routes/routes%.3d-%.2d-%.2d.json' % (params['distr'], int(params['day']), int(params['month']), params['year'], tmphour, tmpminute, ii, tmphour, tmpminute)
            # print(json_file_path)
            if os.path.isfile(json_file_path):
                # print(json_file_path)
                with open(json_file_path) as jsonFile:
                    jsonstr=json.load(jsonFile)
                    # print(jsonstr)
                if jsonstr['status']=='OK':
                    # tp=tp+1
                    # print(jsonstr)
                    # print(jsonstr['routes'][0])

                    legs=jsonstr['routes'][0]['legs']
                    if 'steps' in legs[0]:
                        # if len(legs[0]['steps']) > 1:
                        if legs[0]['distance']['value'] > 100:
                            tp=tp+1

                            # print(ii)
                    # print(legs)
                            # route_info[int(DICT_EDGE[tmpKey])].append(math.ceil(int(summary[0]['travelTimeInSeconds'])/60))# travel time value   
                    if 'duration_in_traffic' in legs[0]:
                        duration = int(legs[0]['duration_in_traffic']['value'])
                        # route_info[ii].append(legs[0]['duration_in_traffic']['value'])# travel time value  
                    else:
                        duration = int(legs[0]['duration']['value'])
                    
                    if duration == 0:
                        duration=1
                    speed= int(legs[0]['distance']['value'])/duration
                    
                    if speed==0:
                        speed=20
                    
                    speed_info[ii].append(speed*3.6)# travel time value  

                    # speed_info[ii].append(math.log(speed*3.6))# travel time value  
                    # print(speed*3.6)
                    route_info[ii].append(duration)# travel time value  
                else:
                    print('status is not OK!!!!!!!')



        print('---------%s----------' % tp)


    # print(route_info)
    route_info_path=('json/duration/duration_info_%s_%.2d-%.2d-%s.npy' % (params['distr'], int(params['day']), int(params['month']), params['year']))
    speed_info_path=('json/speed/speed_info_%s_%.2d-%.2d-%s.npy' % (params['distr'], int(params['day']), int(params['month']), params['year']))
    directory=os.path.dirname(route_info_path)
        # print(directory)
    if not os.path.exists(directory):
        os.makedirs(directory)

    directory=os.path.dirname(speed_info_path)
        # print(directory)
    if not os.path.exists(directory):
        os.makedirs(directory)

    thefile=open(route_info_path, 'w')
    for item in route_info:
        for tmp in range(len(item)):# print(item)
            if tmp < len(item) - 1:
                thefile.write("%s\t" % item[tmp])
            else:
                thefile.write("%s\n" % item[tmp])

    
    thefile=open(speed_info_path, 'w')
    for item in speed_info:
        for tmp in range(len(item)):# print(item)
            if tmp < len(item) - 1:
                thefile.write("%s\t" % item[tmp])
            else:
                thefile.write("%s\n" % item[tmp])
    cnt=-1
    tmpp=0
    for x in route_info:
        cnt = cnt + 1
        std_value = np.std(x)
        avg_value=np.mean(x)
        if std_value/avg_value < 0.5:
            tmpp=tmpp+1
            # print('edge_id:\t%s' % cnt) #edge_id
            # print('std_value:\t%s' % std_value)
            # print(std_value/avg_value)
            # print(x)
            # fig, ax = plt.subplots( nrows=1, ncols=1 )  # create figure & 1 axis
            # ax.plot(x)
            # fig.savefig('Res/img/%s_%s.png' % (params['distr'], cnt))  # save the figure to file
            # plt.close(fig)  
    # print(route_info)
    print(tmpp)

    return route_info

def parse_google_json_to_list_by_day(params,days=1):
    wayReqSet=get_waypointReqSet_from_lat_lng('jti_smp')
    
    for j in range(days): #number of days
        route_info=[] # store travel time
        speed_info=[] # store travel time
        for kk in range(len(wayReqSet)):
            route_info.append([]) 
            speed_info.append([])
        
        hour=params['start_hour']
        minute=params['start_minute']
        interval=params['interval']
        duration=int(params['duration'])
        
        if j > 0:
            params['day']=params['day']+1
        startslot=0
        
        for i in range(duration):   # number of time slots
            if i > 0:
                minute=minute + interval
                tmpminute =int(minute%60)
                tmphour=int(hour + minute/60)
            else:
                minute=minute
                tmpminute =int(minute)
                tmphour=int(hour)     
    
            tp=0
            for ii in range(len(wayReqSet)): # each routing request
                json_file_path='Res/%s/date_%s-%.2d-%.2d/time_%.2d-%.2d/routes/routes%.3d-%.2d-%.2d.json' % (params['distr'],  params['year'], int(params['month']),int(params['day']), tmphour, tmpminute, ii, tmphour, tmpminute)
                # print(json_file_path)
                if os.path.isfile(json_file_path):
                    # print(json_file_path)
                    with open(json_file_path) as jsonFile:
                        jsonstr=json.load(jsonFile)
                        # print(jsonstr)
                    if jsonstr['status']=='OK':
                        # print( '%.2d:%.2d' % (tmphour, tmpminute))
                        legs=jsonstr['routes'][0]['legs']
                        # if 'steps' in legs[0]:
                            # if legs[0]['distance']['value'] > 100:
                                # tp=tp+1
                        if 'duration_in_traffic' in legs[0]:
                            durationv=int(legs[0]['duration_in_traffic']['value'])
                            # route_info[ii].append(legs[0]['duration_in_traffic']['value'])# travel time value  
                        else:
                            durationv=int(legs[0]['duration']['value'])

                        # print( durationv)
                        if i ==0:  
                            speed_info[ii].append(int(legs[0]['distance']['value']))
                            route_info[ii].append(int(legs[0]['distance']['value']))
                            route_info[ii].append(float(float(int(legs[0]['duration']['value']))/60))
                            speed_info[ii].append(float(legs[0]['duration']['value']))

                        # route_info[ii].append(durationv)# travel time value          
                        
                        if durationv == 0:
                            durationv=1
                        speed= int(legs[0]['distance']['value'])/durationv                            
                        if speed==0:
                            speed=20                            
                        # speed_info[ii].append(speed*3.6)# travel time value  
                        # speed_info[ii].append(math.log(speed*3.6))# travel time value  
                        speed_info[ii].append(int(speed*3.6))# travel time value  
                        route_info[ii].append(float(float(durationv/60)))

                        # print(speed*3.6)

                    else:
                        print('link %s status is not OK!!!!!!!!!!!!!' % ii)     
                else:
                    print(json_file_path)
                 

            # print('---------%s----------' % tp)
    
        print('len duration is %s' % len(route_info[0]))
        print(route_info[21])
        route_info_path=('json/all/duration/google_duration_info_%s_%.2d_%.2d_%s-%.2d-%.2d.csv' % (params['distr'],  int(params['start_hour']), int(params['start_minute']), params['year'],int(params['month']), int(params['day'])))
        speed_info_path=('json/all/speed/google_speed_info_%s_%.2d_%.2d_%.2d-%.2d-%.2d.csv' % (params['distr'], int(params['start_hour']), int(params['start_minute']), params['year'], int(params['month']),int(params['day'])))
        directory=os.path.dirname(route_info_path)
            # print(directory)
        if not os.path.exists(directory):
            os.makedirs(directory)        
        directory=os.path.dirname(speed_info_path)
            # print(directory)
        if not os.path.exists(directory):
            os.makedirs(directory)        
       
    ### write durations into file by day
        thefile=open(route_info_path, 'w')
        thefile.write("link_id,distance,typical_duration")    
    
        minute=params['start_minute']
        
        for i in range(duration):   # number of time slots
            # print(i)
            if i > 0:
                minute=minute + interval
                tmpminute =int(minute%60)
                tmphour=int(hour + minute/60)%24
            else:
                minute=minute
                tmpminute =int(minute)
                tmphour=int(hour)     
            thefile.write(',%.2d:%.2d' % (tmphour, tmpminute))
                # print((',%.2d:%.2d' % (tmphour, tmpminute)))
        thefile.write('\n')
    
        linkid=-1
        for item in route_info:
            linkid=linkid+1
            thefile.write("%s," % linkid)
            for tmp in range(len(item)):# print(item)
                if tmp < len(item) - 1:
                    thefile.write("%6.2f," % item[tmp])
                else:
                    thefile.write("%6.2f\n" % item[tmp])                    
    
    
### write speed into file   
        thefile=open(speed_info_path, 'w')
        thefile.write("link_id,distance")    

        for j in range(duration):
            curSlot=startslot+j
            thefile.write(',slot_%.2d' % curSlot)
        thefile.write('\n')
    
        linkid=-1
        for item in speed_info:
            linkid=linkid+1
            thefile.write("%s," % linkid)
            for tmp in range(len(item)):# print(item)
                if tmp < len(item) - 1:
                    thefile.write("%6.2f," % item[tmp])
                else:
                    thefile.write("%6.2f\n" % item[tmp])

    return speed_info
